/**
 * Created by anisha on 29/4/16.
 */

var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var db = require('./db');

router.use(express.static(__dirname + '/public'));

router.get('/', function(req, res) {
    res.render('record');
});

router.get('/:lat/:long', function(req, res) {
    var lat = req.params.lat;
    var long = req.params.long;
    res.render('record',{ lat:lat, long:long });
});

router.post('/:lat/:long', function(req,res){
    var type = String(req.body.type);
    var time = String(req.body.time);
    var lat = String(req.body.lat);
    var long = String(req.body.long);
    var details = String(req.body.details);
    var location = "POINT(" + long + " " + lat + ")";
    var query = {};
    var query2 = {};
    var text = '';
    query.text = "INSERT INTO crime(type,time,geom,details,informalorformal) VALUES ($1,$2, ST_GeomFromText($3,4326), $4, $5)";
    query.values = [type,new Date(),location,details,"informal"];
    db.query(query.text, query.values, function (err, data) {
        console.log(query.text);
        if (err) {
            console.error('error', err);
            res.status(500).send('Invalid Request');
        }
        else {
            console.log('success');
        }

    });
    crimes={
        "Crime1":"c1",
        "Crime2":"c2",
        "Crime3":"c3",
        "Crime4":"c4",
        "Crime5":"c5",
        "Crime6":"c6"};
    var crimetype = crimes[type];
    query2.text="UPDATE ward SET " + crimetype + " =  " +  crimetype  + " +  1 WHERE ST_Contains(geom,ST_GeomFromText('" + location + "',4326));";
    console.log(query.text);
    query2.values = [];
    db.query(query2.text, query2.values, function (err, data) {
        console.log(query2.text);
        if (err) {
            console.error('error', err);
            res.status(500).send('Invalid Request');
        }
        else {
            console.log('success_ward');
        }

    });
    res.redirect("/thankyou");
});

module.exports = router;
