/**
 * Created by anisha on 29/4/16.
 */
/**
 * Created by anisha on 29/4/16.
 */

var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var db = require('./db');

router.use(express.static(__dirname + '/public'));

router.get('/', function(req, res) {
    res.render('sign');
});

router.post('/', function(req,res){
    var user = req.body.user;
    var password = req.body.pass;
    var query = {};
    var text = '';
    query.text = "SELECT username, password FROM user_store WHERE username = '" + user + "'";
    query.values = [];
    db.query(query.text, query.values, function (err, data) {
        if (err) {
            console.error('error', err);
            var url = '/sign/forgot';
            res.status(500).send('Invalid Request');
        }
        else {
            if(data.rows.length == 1) {
                if (data.rows[0].password == password) {
                    var url = '/account/' + user;
                }
                else {
                    var url = '/sign/forgot';
                }
            }
            else
            {
                var url = '/sign/forgot';
            }
            res.send({url: url});
            //return res.redirect(url);
        }
    });
});

router.get('/sign/forgot', function(req,res){
    res.render('forgot')
});


router.post('/sign/forgot/:user', function(req,res){
    var user = req.body.user;
    var secret = req.body.number;
    return res.redirect('/forgot_complete');
});
module.exports = router;
